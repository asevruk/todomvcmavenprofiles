package net.itlabs;

import net.itlabs.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static net.itlabs.pages.ToDoMVC.*;

/**
 * Created by ������ on 13.02.2016.
 */

@Category(Smoke.class)
public  class ToDoE2ETest extends BaseTest {
    @Test
    public void testE2ESmokeTest() {
        givenAtAll();

        create("a", "b");
        assertVisibleTasks("a", "b");

        //complete
        toggle("a");
        assertVisibleTasks("a", "b");

        openActive();

        startEdit("b", "b edited").pressEnter();
        assertVisibleTasks("b edited");

        openCompleted();

        //reopen
        toggle("a");
        assertVisibleTasksAreEmpty();

        openAll();
        assertVisibleTasks("a", "b edited");

        delete("a");
        assertVisibleTasks("b edited");

        toggleAll();
        assertVisibleTasks("b edited");

        clearCompleted();
        assertVisibleTasksAreEmpty();
    }


}

