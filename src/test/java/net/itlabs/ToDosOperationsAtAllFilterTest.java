package net.itlabs;

import net.itlabs.categories.Buggy;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.codeborne.selenide.Condition.disappear;
import static net.itlabs.pages.ToDoMVC.Task.aTask;
import static net.itlabs.pages.ToDoMVC.Type.COMPLETED;
import static net.itlabs.pages.ToDoMVC.*;

/**
 * Created by ������ on 13.02.2016.
 */

public class ToDosOperationsAtAllFilterTest extends BaseTest {
    @Test
    public void testCreateOnAll() {
        givenAtAll();

        create("a");

        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test 
    public void testEditOnAll() {
        givenAtAll("a", "b");

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited", "b");
        assertItemsLeft(2);
    }

    @Test 
    public void testDeleteOnAll() {
        givenAtAll("a", "b");

        delete("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test 
    public void testCompleteOnAll() {
        givenAtAll("a", "b");

        toggle("a");

        assertVisibleTasks("a", "b");
        assertItemsLeft(1);

    }

    @Test 
    public void testCompleteAllOnAll() {
        givenAtAll(aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        toggleAll();
        assertVisibleTasks("a", "b", "c");
        assertItemsLeft(0);

    }

    @Test 
    public void testClearCompletedOnAll() {
        givenAtAll(aTask("a", COMPLETED),
                aTask("c"));

        clearCompleted();
        assertVisibleTasks("c");
        assertItemsLeft(1);
    }

    @Test 
    public void testReopenOnAll() {
        givenAtAll(
                aTask("a", COMPLETED),
                aTask("b"));

        toggle("a");
        assertVisibleTasks("a", "b");
        assertItemsLeft(2);
        clearCompltetedButton.should(disappear);
    }

    @Test 
    public void testReopenAllOnAll() {
        givenAtAll(
                aTask("a", COMPLETED),
                aTask("c", COMPLETED));

        toggleAll();

        assertVisibleTasks("a", "c");
        assertItemsLeft(2);
        clearCompltetedButton.should(disappear);
    }

    @Test 
    public void testCancelEditOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test 
    public void testEditAndClickOutsideOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test 
    public void testDeleteByEditingOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "").pressEnter();
        assertVisibleTasksAreEmpty();
    }

    @Test 
    public void testFilterToActiveFromAll() {
        givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        openActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test 
    public void testFilterToCompletedFromAll() {
        givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        openCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Category(Buggy.class)
    @Test
    public void testWithBugDeleteOnAll() {
        givenAtAll("a", "b");

        delete("a");
        assertVisibleTasks("b");
        assertItemsLeft(2);
    }
}





